(ns muas.core
  (:gen-class)
  (:require [clj-http.client :as client])
  (:require [clojure.java.browse :as browse])
  (:use [clojure.string :only (join)]))

(defn -main
  "Muas, the best friend in your shell."
  [& args]

  ;; Variables
  (def newsapi-hn 
    "https://newsapi.org/v2/everything?sources=hacker-news&apiKey=6e4555f42bd948b58d60eaf30deca94f")

  (def newsapi-rd
    "https://newsapi.org/v2/top-headlines?sources=reddit-r-all&apiKey=6e4555f42bd948b58d60eaf30deca94f")

  (def newsapi-gg
    "https://newsapi.org/v2/top-headlines?sources=google-news&apiKey=6e4555f42bd948b58d60eaf30deca94f")

  (def googleapit
    "https://translate.googleapis.com/translate_a/single?client=gtx&sl=")
  
  (let [farg (first args)
        args (rest args) ]
    (cond
      (nil? farg) ;; No arguments
        (println "Usage\n"
                 "news (feed) - Shows news from (feed).\n"
                 "50          - Show 1 or 0 by %50 chance.\n"
                 "search (engine) - Search by specific engine in the browser.\n")

      (= farg "news") ;; News feed
        (cond
          (= (first args) "hackernews")
            (doseq [arg (get-in (client/get newsapi-hn {:as :json}) [:body :articles])]
              (println (str (:title arg) "\n")
                       (str (:description arg) "\n")
                       (str (:url arg) " by " (:author arg) "\n")))
          (= (first args) "reddit")
            (doseq [arg (get-in (client/get newsapi-rd {:as :json}) [:body :articles])]
              (println (str (:title arg) "\n")
                       (str (:description arg "\n"))
                       (str (:url arg) "\n")))
          (= (first args) "google")
            (doseq [arg (get-in (client/get newsapi-gg {:as :json}) [:body :articles])]
              (println (str (:title arg) "\n")
                       (str (:description arg "\n"))
                       (str (:url arg) "\n"))))

      (= farg "search") ;; Web search
        (let [engine   (first args)
              keywords (join " " (rest args))] 
          (cond
            (= engine "lucky")
              (browse/browse-url (str "https://www.google.com/search?btnI=I%27m+Feeling+Lucky&q=" keywords))
            (= engine "google")
              (browse/browse-url (str "https://www.google.com.tr/search?q=" keywords))
            (= engine "youtube")
              (browse/browse-url (str "https://www.youtube.com/results?search_query=" keywords))
            (= engine "maps")
              (browse/browse-url (str "https://www.google.com.tr/maps/place/" keywords))
            (= engine "books")
              (browse/browse-url (str "https://www.google.com/search?tbm=bks&q=" keywords))
            :else
              (browse/browse-url (str "https://www.google.com.tr/search?q=" (join " " args)))))
 
      (or (= farg "translate") (= farg "t"))
        (do
          (let [api (str googleapit 
                         (first args) 
                         "&tl=" 
                         (second args) 
                         "&dt=t&q="
                         (join " " (drop 2 args)))]
          (println "Translation of \"" (join " " (drop 2 args)) "\" is \"" 
                   (get-in (client/get googleapit {:as :json}) [0 0 ]) "\"")))

      (= farg "50") ;; 1 or 0 by 50 present chance
        (if (= (mod (rand-int 100) 2) 0)
          (println "Its 1")
          (println "Its 0")))))
