(defproject muas "0.1.0-SNAPSHOT"
  :description "Assistant for your terminal."
  :url "https://github.com/midnio/muas"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.8.0"] [clj-http "3.9.1"] [cheshire "5.8.0"]]
  :main ^:skip-aot muas.core
  :target-path "target/%s"
  :profiles {:uberjar {:aot :all}})
